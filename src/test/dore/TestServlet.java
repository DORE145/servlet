package test.dore;

import java.io.BufferedReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


/**
 * Servlet implementation class TestServlet
 */
@WebServlet("/TestServlet")
public class TestServlet extends HttpServlet {
	
	private static final String TABLE_NAME = "\"data\".\"monitoring::monitoring\"";
	
	private DataSource dataSource;
	private String message;
	private List<Long> perfomance;
	private List<String> columnsNames; 
	private Set<String> errors;
       
    /**
	 * 
	 */
	private static final long serialVersionUID = 3549546637055556178L;

	@Override
	public void init() throws ServletException {
		super.init();
		perfomance = new LinkedList<>();
		errors = new HashSet<>();
        try {
			InitialContext ctx = new InitialContext();
			dataSource = (DataSource)ctx.lookup("java:comp/env/jdbc/stats");
			message = "Connected to databse. \n";
		} catch (Exception e) {
			message = "Connection failed. " + e.getMessage();
			throw new ServletException(e);
		} 
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) 
			throws ServletException, IOException {
		Connection connection = null;
		try {
			connection = getConnecrion();
			String query = "SELECT count(*) FROM " + TABLE_NAME;
			StringBuilder builder = new StringBuilder("\n");
			Statement statement = connection.createStatement();
			ResultSet set = statement.executeQuery(query);
			builder.append("Executing query: " + query + "\n");
			builder.append("Table contents: ");
			set.next();
			builder.append(set.getInt(1) + "\n");
			statement.close();
			if (!perfomance.isEmpty()) {
				long total = 0;
				for (Long l : perfomance) {
					total += l;
				}
				double average = (total * 1.0)/ perfomance.size();
				builder.append("Time total for all inserts: " + total + "ms\n");
				builder.append("Average for 1 insert; " + average + "ms\n");
			}
			if (!errors.isEmpty()) {
				builder.append("Errors: ").append("\n");
				for (String error : errors) {
					builder.append(error).append("\n");
				}
			}
			message = message + builder.toString();
		} catch (Exception e) {
			message = message + "Data retrieving error. " + e.getMessage();
			// TODO: handle exception
		} finally {
			if (connection != null) {
				try {
					connection.close();
				} catch (SQLException ignored) {}
				response.resetBuffer();
				response.getWriter().append("Served at: ").append(request.getContextPath());
				response.getWriter().append("\n" + message + "\n");
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
		Connection connection = null;
		String jsonString = null;
		try {
			connection = getConnecrion();
			long startTimestamp = new java.util.Date().getTime();
			//Reading json sting from request
			BufferedReader requestReader = request.getReader();
			StringBuilder jsonBuilder = new StringBuilder();
			while (requestReader.ready()) {
				jsonBuilder.append(requestReader.readLine());
			}
			jsonString = jsonBuilder.toString();

			//Necessary to specify column names to prevent possible errors and because columns in HANA displaying in different order
			String query = "INSERT INTO " + TABLE_NAME + " "
					+ getColumnsNames()
					+ generateValuesPart(getColumnsNamesList());
			response.getWriter().println(query);
			PreparedStatement statement = connection.prepareStatement(query);
			statement = completeStatement(jsonString, statement);
			statement.execute();
			statement.close();
			long endTimestamp = new java.util.Date().getTime();
			perfomance.add(endTimestamp - startTimestamp);
			response.getWriter().println("Query executed in " + (endTimestamp - startTimestamp) + "ms.");
			
		} catch (SQLException e) {
				response.getWriter().println("Failed to create or execute Prepared Statement: " + e.getMessage());
				response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				response.getWriter().flush();
				errors.add("Failed to create or execute Prepared Statement: " + e.getMessage());
		} catch (ParseException e) {
				response.getWriter().println("Failed to parse json: " + e.getMessage());
				if (jsonString.isEmpty() ) {
					response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
				} else {
					response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				}
				response.getWriter().flush();
				errors.add("Failed to parse json: " + e.getUnexpectedObject().toString());
		} catch (java.text.ParseException e) {
				response.getWriter().println("Failed to parse values: " + e.getMessage());
				response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				response.getWriter().flush();
				errors.add("Failed to parse values: " + e.getMessage());
		} finally {
			if (connection != null) {
				try { 
					connection.close();
				} catch (SQLException ignored) {}
			}
		}
	}
	
	private List<String> getColumnsNamesList() {
		if (columnsNames == null) {
			columnsNames = new LinkedList<>();
			columnsNames.add("Date");
			columnsNames.add("Time");
			columnsNames.add("AS.Instance");
			columnsNames.add("Act.WPs");
			columnsNames.add("Dia.WPs");
			columnsNames.add("RFC.WPs");
			columnsNames.add("CPU.Usr");
			columnsNames.add("CPU.Sys");
			columnsNames.add("CPU.Idle");
			columnsNames.add("Paging.in");
			columnsNames.add("Paging.out");
			columnsNames.add("Free.Mem");
			columnsNames.add("EM.alloc");
			columnsNames.add("EM.attach");
			columnsNames.add("EM.global");
			columnsNames.add("Heap.Memor");
			columnsNames.add("Pri");
			columnsNames.add("Paging.Mem");
			columnsNames.add("Dia");
			columnsNames.add("Upd");
			columnsNames.add("Enq");
			columnsNames.add("Logins");
			columnsNames.add("Sessions");
			columnsNames.add("Roll.Mem");
			columnsNames.add("Queue");
			columnsNames.add("Queue2");
			columnsNames.add("Monitoring.GUID");
		}
		return columnsNames;
	}
	
	private String getColumnsNames() {
		List<String> list = getColumnsNamesList();
		StringBuilder builder = new StringBuilder("(");
		for (String item : list) {
			builder.append("\"").append(item).append("\"").append(", ");
		}
		builder.replace(builder.length() - 2, builder.length(), ") ");
		return builder.toString();
	}
	
	private String generateValuesPart(List<String> values) {
		StringBuilder builder = new StringBuilder("VALUES (");
		for (int i = 0; i < values.size(); i++){
			builder.append("?");
			if (i != values.size() - 1) {
				builder.append(", ");
			}
		}
		builder.append(")");
		return builder.toString();
	}
	
	private PreparedStatement completeStatement(String jsonString, PreparedStatement statement) throws ParseException, java.text.ParseException, SQLException {
		JSONObject json = (JSONObject) new JSONParser().parse(jsonString);			
		List<String> columns = getColumnsNamesList();
		for (int i = 0; i < columns.size(); i++) {
			String item = columns.get(i);
			if (item.equals("Date")) {
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
				String dateString = (String) json.get(item);
				Date date = new Date(dateFormat.parse(dateString).getTime());
				statement.setDate(i + 1, date);
			} else if (item.equals("Time")) {
				SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
				String timeString = (String) json.get(item);
				Time time = new Time(timeFormat.parse(timeString).getTime());
				statement.setTime(i + 1, time);
			} else if (item.equals("AS.Instance") || item.equals("Monitoring.GUID")) {
				String dataString = (String)json.get(item);
				statement.setString(i + 1, dataString);
			} else {
				String integerString = (String) json.get(item);
				int integer = NumberFormat.getIntegerInstance(Locale.US).parse(integerString).intValue();
				statement.setInt(i + 1, integer);
			}
		}
		return statement;
	}
	
	private Connection getConnecrion() throws SQLException{
		return dataSource.getConnection();
	}
}